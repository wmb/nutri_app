#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    const char *msg = argc > 1 ? argv[1] : "Hello, world!";

    puts(msg);

    exit(EXIT_SUCCESS);
}