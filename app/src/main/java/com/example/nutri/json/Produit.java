package com.example.nutri.json;


import androidx.annotation.NonNull;

import java.util.Locale;

@SuppressWarnings("unused")
public class Produit {
    final int code;
    final String nom;
    final String description;
    final String codebarre;

    public Produit() {
        this(-1, "", "", "");
    }

    Produit(int code, String nom, String description, String codebarre) {
        this.code = code;
        this.nom = nom;
        this.description = description;
        this.codebarre = codebarre;
    }

    public int getCode() {
        return code;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public String getCodebarre() {
        return codebarre;
    }

    @Override
    @NonNull
    public String toString() {
        return String.format(Locale.getDefault(), "[Produit %d %s]", code, nom);
    }
}
