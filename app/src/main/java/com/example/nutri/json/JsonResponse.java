package com.example.nutri.json;

public class JsonResponse {
    private final JSONResponseError error;

    public JsonResponse() {
        this.error = new JSONResponseError();
    }

    public JsonResponse(JsonResponse jsonResponse) {
        super();
        if (jsonResponse != null)
            this.error = new JSONResponseError(jsonResponse.isError(), jsonResponse.getMsg());
        else
            error = new JSONResponseError();
    }

    public boolean isError() {
        return error.isError();
    }

    public String getMsg() {
        return error.getMsg();
    }

    private static class JSONResponseError {
        private final boolean error;
        private final String msg;

        JSONResponseError() {
            this.error = false;
            this.msg = "";
        }

        JSONResponseError(boolean error, String msg) {
            this.error = error;
            this.msg = msg;
        }

        boolean isError() {
            return error;
        }

        String getMsg() {
            return msg;
        }
    }
}
