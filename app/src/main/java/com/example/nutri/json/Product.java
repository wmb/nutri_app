package com.example.nutri.json;

import android.os.Parcel;
import android.os.Parcelable;

public class Product extends Produit implements Parcelable {
    public static final String EXTRA_PRODUCT = "com.example.nutri.json.Product";
    public static final Parcelable.Creator<Product> CREATOR
            = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            int code = source.readInt();
            String[] others = new String[3];
            source.readStringArray(others);
            return new Product(code, others);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    private Product(int code, String nom, String description, String codebarre) {
        super(code, nom, description, codebarre);
    }

    private Product(int code, String[] a) {
        this(code, a[0], a[1], a[2]);
    }

    public static Product from(Produit produit) {
        return new Product(produit.code, produit.nom, produit.description, produit.codebarre);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeStringArray(new String[]{nom, description, codebarre});
    }
}
