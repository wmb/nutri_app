package com.example.nutri.json;


@SuppressWarnings("unused")
public class JsonSearchProductResponse extends JsonResponse {
    private final String search;
    private final int nresults;
    private final Produit[] results;

    public JsonSearchProductResponse() {
        this("", -1, null);
    }

    private JsonSearchProductResponse(String search, int nresults, Produit[] results) {
        super();
        this.search = search;
        this.nresults = nresults;
        this.results = results;
    }

    private JsonSearchProductResponse(JsonResponse jsonResponse, String search, int nresults, Produit[] results) {
        super(jsonResponse);
        this.search = search;
        this.nresults = nresults;
        this.results = results;
    }

    public String getSearch() {
        return search;
    }

    public int getNresults() {
        return nresults;
    }

    public Produit[] getResults() {
        return results;
    }

}
