package com.example.nutri;

import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.example.nutri.http.Store;
import com.example.nutri.json.JsonResponse;
import com.example.nutri.json.JsonSearchProductResponse;
import com.example.nutri.json.Produit;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.security.auth.login.LoginException;

class NutriAPI {
    private static final String TAG = "NutriAPI";
    private static final int TIMEOUT = 2_000; /* Timeout in 2 seconds */
    private static final Gson gson = new Gson();
    private final Uri BASERUI;

    NutriAPI(String baseUrl) {
        this.BASERUI = Uri.parse(baseUrl);
    }

    Produit[] produits(String query) throws IOException, NutriAPIException {
        return search(query).getResults();
    }

    void login(String user, String pass) throws IOException, LoginException {
        JsonResponse response = gson.fromJson(loginInputStream(user, pass), JsonResponse.class);
        if (response.isError())
            throw new LoginException("Login failed: " + response.getMsg());
    }

    void addCatProd(String name, String desc) throws IOException, NutriAPIException {
        JsonResponse jsonResponse = gson.fromJson(addCatProdInputStream(name, desc), JsonResponse.class);
        if (jsonResponse.isError())
            throw new NutriAPIException(jsonResponse);
    }

    void signup(String user, String name, String mail, String pass) throws IOException, NullPointerException, NutriAPIException {
        final Reader reader = signupInputStream(user, name, mail, pass);
        final JsonResponse response = gson.fromJson(reader, JsonResponse.class);

        if (response.isError())
            throw new NutriAPIException(response);
    }

    private JsonSearchProductResponse search(String query) throws IOException, NutriAPIException {
        JsonSearchProductResponse json = gson.fromJson(searchInputStream(query), JsonSearchProductResponse.class);
        if (json.isError())
            throw new NutriAPIException(json);
        Log.d(TAG, "Decoded JSON: " + gson.toJson(json));
        return json;
    }

    private InputStreamReader searchInputStream(String query) throws IOException {
        URL url = param("search", "q", query);
        Log.d(TAG, "Search request url: " + url);
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URLConnection con = url.openConnection();
        con.setConnectTimeout(TIMEOUT);
        con.connect();
        return new InputStreamReader(con.getInputStream());
    }

    private InputStreamReader loginInputStream(String user, String pass) throws IOException {
        HashMap<String, String> map = new HashMap<>(2);
        map.put("user", user);
        map.put("pass", pass);
        URL url = param("login", map);
        Log.d(TAG, "Login request url: " + url);
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URLConnection con = url.openConnection();
        putCookies(con, url);
        con.setConnectTimeout(TIMEOUT);
        con.connect();
        List<String> cookies = con.getHeaderFields().get("Set-Cookie");
        Store.add(url.getHost(), cookies);
        return new InputStreamReader(con.getInputStream());
    }

    private InputStreamReader signupInputStream(String user, String name, String mail, String pass) throws IOException, NullPointerException {
        if (user == null || name == null || mail == null || pass == null)
            throw new NullPointerException("One of the arguments was null");

        HashMap<String, String> params = new HashMap<>(4);
        params.put("user", user);
        params.put("name", name);
        params.put("mail", mail);
        params.put("pass", pass);
        URL url = param("/adduser", params);
        Log.d(TAG, "signupInputStream: signup url: " + url);
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URLConnection con = url.openConnection();
        con.setConnectTimeout(TIMEOUT);
        con.connect();
        return new InputStreamReader(con.getInputStream());
    }

    private InputStreamReader addCatProdInputStream(String name, String desc) throws IOException {
        Map<String, String> map = new HashMap<>(2);
        map.put("name", name);
        map.put("desc", desc);
        URL url = param("/addcatprod", map);
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        URLConnection con = url.openConnection();
        con.setConnectTimeout(TIMEOUT);
        con.connect();
        if (((HttpURLConnection) con).getResponseCode() % 100 == 2) {
            return new InputStreamReader(con.getInputStream());
        }
        return new InputStreamReader(((HttpURLConnection) con).getErrorStream());
    }

    private void putCookies(URLConnection con, URL uri) {
        List<HttpCookie> cookies_to_put = Store.getCookies(uri.getHost());
        if (cookies_to_put == null)
            return;
        for (HttpCookie cookie : cookies_to_put) {
            String cookieHeader = cookie.getName() + '=' + cookie.getValue();
            con.setRequestProperty("Cookie", cookieHeader);
        }
    }

    @SuppressWarnings("SameParameterValue")
    private URL param(String endpoint, String key, String value) throws MalformedURLException {
        HashMap<String, String> map = new HashMap<>(1);
        map.put(key, value);
        return param(endpoint, map);
    }

    private URL param(String endpoint, Map<String, String> map) throws MalformedURLException {
        Uri.Builder uri = BASERUI.buildUpon().path(endpoint);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            map.forEach(uri::appendQueryParameter);
        else
            for (String key : map.keySet())
                uri.appendQueryParameter(key, Objects.requireNonNull(map.get(key)));

        try {
            return new URL(uri.toString());
        } catch (MalformedURLException e) {
            Log.e(TAG, "param: Malformed URL: " + uri.toString(), e);
            e.printStackTrace();
            throw e;
        }
    }
}
