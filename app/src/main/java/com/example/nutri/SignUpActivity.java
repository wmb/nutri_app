package com.example.nutri;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.arch.core.util.Function;
import androidx.constraintlayout.widget.Group;

import com.example.nutri.util.MyAppCompatActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class SignUpActivity extends MyAppCompatActivity {
    private static final int NUMBER_OF_EDITTEXTS = 4;
    private static final String TAG = "SignUpActivity";
    private final HashMap<String, EditText> ets = new HashMap<>(NUMBER_OF_EDITTEXTS);
    private Group loginGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        findViews();
    }

    @SuppressWarnings("RedundantCast")
    @Override
    protected void findViews() {
        super.findViews();
        ets.put("user", (EditText) findViewById(R.id.etuser));
        ets.put("name", (EditText) findViewById(R.id.etname));
        ets.put("mail", (EditText) findViewById(R.id.etmail));
        ets.put("pass", (EditText) findViewById(R.id.etpass));
        loginGroup = findViewById(R.id.login_group);
    }

    private <T, V> T[] filter_keys(Map<T, V> map, @SuppressWarnings("SameParameterValue") Class<T> keyClass, Predicate<V> filter) {
        if (map == null)
            return null;

        if (filter == null)
            //noinspection unchecked
            return (T[]) map.keySet().toArray();

        Set<T> filtered = new HashSet<>(map.size());

        for (T key : map.keySet())
            if (filter.test(map.get(key)))
                filtered.add(key);

        //noinspection unchecked
        return filtered.toArray((T[]) Array.newInstance(keyClass, 0));
    }

    private String[] unfilled_fields() {
        return filter_keys(ets, String.class, tv -> tv == null || tv.length() == 0);
    }

    private boolean toast_unfilled_fields() {
        String[] unfilled = unfilled_fields();

        if (unfilled == null || unfilled.length == 0)
            return false;

        StringBuilder fields = new StringBuilder();
        for (int i = 0; i < unfilled.length - 1; ++i) {
            fields.append(unfilled[i]);
            fields.append(' ');
            fields.append(getString(R.string.and));
            fields.append(' ');
        }
        fields.append(unfilled[unfilled.length - 1]);
        toast_short(String.format(Locale.getDefault(),
                getString(R.string.please_fill_filed), fields.toString()));

        return true;
    }

    public void signUpButtonOnClickListener(View view) {
        if (toast_unfilled_fields())
            return;

        view.setEnabled(false);

        AsyncSignupNutriClient asnc = new AsyncSignupNutriClient(
                getString(R.string.API_BASEURL),
                this::signupCallback,
                new WeakReference<>(view)
        );

        final Function<TextView, String> map = tv -> tv.getText().toString();
        asnc.execute(
                map.apply(ets.get("user")),
                map.apply(ets.get("name")),
                map.apply(ets.get("mail")),
                map.apply(ets.get("pass"))
        );
    }

    private void signupCallback(Exception exception, WeakReference<View> buttonRef) {
        if (buttonRef != null)
            buttonRef.get().setEnabled(true);

        if (exception != null) {
            try {
                throw exception;
            } catch (SocketTimeoutException e) {
                Log.d(TAG, "signupCallback: timeout", e);
                toast_long(getString(R.string.timeout_error), e);
            } catch (IOException e) {
                Log.d(TAG, "signupCallback: io error", e);
                toast_long(getString(R.string.io_error), e);
            } catch (NullPointerException e) {
                Log.d(TAG, "signupCallback: null argument(s)", e);
                toast_long(getString(R.string.argument_error), e);
            } catch (NutriAPIException e) {
                Log.d(TAG, "signupCallback: server error", e);
                toast_long(getString(R.string.server_returned_error), e);
            } catch (Exception e) {
                Log.d(TAG, "signupCallback: unhandled error", e);
                toast_long(getString(R.string.unhandled_exception), e);
            }
            return;
        }

        toast_long(getString(R.string.signup_success));

        loginGroup.setVisibility(View.VISIBLE);
    }

    public void goToLoginButtonOnClickListener(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);

        Log.d(TAG, "goToLoginButtonOnClickListener: starting MainActivity");
        try {
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "goToLoginButtonOnClickListener: Failed to start MainActivity", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return default_onmenuitemselected(this, item, null);
    }

    interface Predicate<T> {
        boolean test(T v);
    }
}
