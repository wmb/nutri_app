package com.example.nutri;

import com.example.nutri.json.JsonResponse;

class NutriAPIException extends Exception {
    private static final String MESSAGE = "An error occurred.";

    NutriAPIException(JsonResponse jsonResponse) {
        super(jsonResponse != null ?
                jsonResponse.getMsg() != null ?
                        jsonResponse.getMsg()
                        : MESSAGE
                : MESSAGE);
    }
}
