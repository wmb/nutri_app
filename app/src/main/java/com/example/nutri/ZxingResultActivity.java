package com.example.nutri;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.nutri.util.MyAppCompatActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ZxingResultActivity extends MyAppCompatActivity {
    private static final String TAG = "ZxingResultActivity";
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zxing_result);
    }

    @Override
    protected void findViews() {
        super.findViews();
        textView = findViewById(R.id.textView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return default_onmenuitemselected(this, item, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            textView.setText(scanResult.getContents());
            return;
        }
        Log.w(TAG, "onActivityResult: scanResult was null");
        super.onActivityResult(requestCode, resultCode, data);
    }
}
