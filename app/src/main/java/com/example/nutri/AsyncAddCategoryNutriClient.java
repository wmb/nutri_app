package com.example.nutri;

import android.os.AsyncTask;

import java.io.IOException;

class AsyncAddCategoryNutriClient extends AsyncTask<String, Void, Exception> {
    private final String BASE_URL;
    private final Callbacks callbacks;

    AsyncAddCategoryNutriClient(String baseUrl, Callbacks callbacks) {
        this.BASE_URL = baseUrl;
        this.callbacks = callbacks;
    }

    protected void onPreExecute() {
        callbacks.onPreExecute();
    }

    @Override
    protected Exception doInBackground(String... strings) {
        if (strings.length != 2)
            return new IllegalArgumentException("AddCategoryAsyncTask: wrong number of arguments");
        try {
            do_add_catp(strings[0], strings[1]);
        } catch (Exception e) {
            return e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Exception e) {
        callbacks.onPostExecute(e);
    }

    private void do_add_catp(String name, String desc) throws IOException, NutriAPIException {
        (new NutriAPI(BASE_URL)).addCatProd(name, desc);
    }

    interface Callbacks {
        void onPreExecute();

        void onPostExecute(Exception exception);
    }
}
