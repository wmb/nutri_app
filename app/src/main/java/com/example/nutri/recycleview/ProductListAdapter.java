package com.example.nutri.recycleview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutri.R;
import com.example.nutri.json.Produit;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {
    private final List<Produit> productList;
    private final ProductListItemOnClick mOnClick;

    public ProductListAdapter(List<Produit> productList, ProductListItemOnClick onClick) {
        this.productList = productList;
        mOnClick = onClick;
    }

    @NonNull
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cv = LayoutInflater.from(parent.getContext()).inflate(R.layout.produit, parent, false);
        return new ProductListViewHolder(cv, mOnClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder viewHolder, int i) {
        Produit produit = productList.get(i);

        viewHolder.getTitle().setText(produit.getNom());
        viewHolder.getDescription().setText(produit.getDescription());
        viewHolder.getBarcode().setText(produit.getCodebarre());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class ProductListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @NonNull
        private final TextView title;
        @NonNull
        private final TextView barcode;
        private final TextView description;

        private final ProductListItemOnClick mOnClick;

        ProductListViewHolder(@NonNull View itemView, ProductListItemOnClick onClick) {
            super(itemView);
            title = itemView.findViewById(R.id.rv_item_product_title);
            barcode = itemView.findViewById(R.id.rv_item_product_barcode);
            description = itemView.findViewById(R.id.rv_item_product_description);
            mOnClick = onClick;
            itemView.setOnClickListener(this);
        }

        @NonNull
        TextView getTitle() {
            return title;
        }

        @NonNull
        TextView getBarcode() {
            return barcode;
        }

        TextView getDescription() {
            return description;
        }

        @Override
        public void onClick(View v) {
            mOnClick.onClick(getAdapterPosition());
        }
    }

    public interface ProductListItemOnClick {
        void onClick(int position);
    }
}
