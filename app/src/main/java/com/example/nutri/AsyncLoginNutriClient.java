package com.example.nutri;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.security.auth.login.LoginException;

class AsyncLoginNutriClient extends AsyncTask<String, Void, Boolean> {
    private static final String TAG = "AsyncLoginNutriClient";
    final private String baseURL;
    final private Callback callback;
    final private WeakReference<Button> buttonWeakReference;
    private String user;
    private Exception exception = null;

    AsyncLoginNutriClient(String baseURL, Callback callback, WeakReference<Button> button) {
        this.baseURL = baseURL;
        this.callback = callback;
        this.buttonWeakReference = button;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);
        callback.callback(user, success, exception, buttonWeakReference);
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        this.user = strings[0];
        String pass = strings[1];

        NutriAPI nc = new NutriAPI(baseURL);

        try {
            nc.login(user, pass);
        } catch (IOException e) {
            exception = e;
            Log.d(TAG, "Login error", e);
            return false;
        } catch (LoginException e) {
            exception = e;
            Log.d(TAG, "Login API error", e);
            return false;
        }

        return true;
    }

    public interface Callback {
        void callback(String user, boolean success, Exception exception, WeakReference<Button> buttonWeakReference);
    }
}
