package com.example.nutri;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nutri.http.Store;
import com.example.nutri.util.MyAppCompatActivity;
import com.example.nutri.util.Utils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import javax.security.auth.login.LoginException;

public class LoginActivity extends MyAppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText username;
    private EditText password;
    private TextView loggedInAs;
    private boolean isLoggedIn = false;
    private String loggedInUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViews();

    }

    @Override
    protected void findViews() {
        super.findViews();
        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        loggedInAs = findViewById(R.id.logged_in_as);
    }

    public void loginButtonOnClickListener(View view) {
        String user = username.getText().toString();
        String pass = password.getText().toString();

        if (user.isEmpty()) {
            toast(getString(R.string.fill_user));
            return;
        }
        if (pass.isEmpty()) {
            toast(getString(R.string.fill_pass));
            return;
        }

        AsyncLoginNutriClient alnc = new AsyncLoginNutriClient(getString(R.string.API_BASEURL), this::loginCallback, new WeakReference<>((Button) view));

        alnc.execute(user, pass);
        view.setEnabled(false);
    }

    private void loginCallback(String user, boolean success, Exception exception, WeakReference<Button> buttonWeakReference) {
        if (buttonWeakReference != null) {
            buttonWeakReference.get().setEnabled(true);
            Log.d(TAG, "loginCallback: buttonWeakReference is not null.");
        } else {
            Log.d(TAG, "loginCallback: buttonWeakReference is null.");
        }

        try {
            if (exception != null)
                throw exception;
        } catch (IOException e) {
            toast_long(getString(R.string.some_error), e);
            Log.d(TAG, "Login error", e);
            return;
        } catch (LoginException e) {
            toast_long(getString(R.string.login_api_error), e);
            Log.d(TAG, "Login API error", e);
            return;
        } catch (Exception e) {
            toast_long(getString(R.string.some_error), e);
            Log.d(TAG, "Unhandled exception", e);
        }

        if (success) {
            loggedInAs.setText(getString(R.string.logged_in_as));
            loggedInAs.append(" " + user);
            toast(getString(R.string.now_logged_in), user);
            isLoggedIn = true;
            loggedInUser = user;
            try {
                String cookie = Utils.listToString(Store.getCookies((new URL(getString(R.string.API_BASEURL))).getHost()));
                ((TextView) findViewById(R.id.signup_text)).setText(cookie);
            } catch (MalformedURLException e) {
                Log.e(TAG, "loginCallback: malformed url", e);
            }
        } else if (exception == null) {
            toast(getString(R.string.login_api_error));
        }
    }

    public void goToSearchButtonOnClickListener(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        Bundle bundle = new Bundle(isLoggedIn ? 2 : 1);
        bundle.putBoolean("isLoggedIn", isLoggedIn);
        if (isLoggedIn) {
            bundle.putString("user", loggedInUser);
        }

        intent.putExtra("loginInfo", bundle);

        Log.d(TAG, "goToSearchButtonOnClickListener: starting MainActivity");
        try {
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "goToSearchButtonOnClickListener: Failed to start MainActivity", e);
        }
    }

    public void goToSignupButtonOnClickListener(View view) {
        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);

        Log.d(TAG, "goToSignupButtonOnClickListener: starting MainActivity");
        try {
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "goToSignupButtonOnClickListener: Failed to start MainActivity", e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret = default_onmenuitemselected(this, item, new int[]{
                R.id.new_account_menu_item,
                R.id.login_menu_item,
                R.id.search_menu_item
        });

        if (ret)
            return true;

        switch (item.getItemId()) {
            case R.id.search_menu_item:
                findViewById(R.id.search_button).performClick();
                ret = true;
                break;
            case R.id.login_menu_item:
                findViewById(R.id.login_button).performClick();
                ret = true;
                break;
            case R.id.new_account_menu_item:
                findViewById(R.id.signup_button).performClick();
                ret = true;
                break;
        }

        return ret;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu, R.menu.login_menu, new int[]{R.id.add_category_menu_item});
    }
}
