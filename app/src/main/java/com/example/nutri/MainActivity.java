package com.example.nutri;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nutri.json.Product;
import com.example.nutri.json.Produit;
import com.example.nutri.recycleview.ProductListAdapter;
import com.example.nutri.util.MyAppCompatActivity;
import com.example.nutri.util.Utils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends MyAppCompatActivity {
    private static final String TAG = "MainActivity";
    private final List<Produit> productList = new LinkedList<>();
    private Button btnsearch;
    private EditText etsearch;
    private ProgressBar pbsearch;
    private RecyclerView rvplist;
    private TextView tvloggedinas;
    private AsyncNutriClient nc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Get the various views from this activity */
        findViews();

        /* Tell the user whether he's logged in or not */
        setUpLoggedIn();

        /* Set layout manager for the RecyclerView */
        rvplist.setLayoutManager(new LinearLayoutManager(this));

        /* Set adapter for the RecyclerView */
        RecyclerView.Adapter rvaplist = new ProductListAdapter(productList, this::onRecyclerViewItemClick);
        rvplist.setAdapter(rvaplist);
        rvplist.setHasFixedSize(true);

        /* Handle the "search" button on the softkeyboard */
        etsearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                /* FIXME: Keyboard should be closed, and search performed when the user clicks on the "search" key. */
                btnsearch.performClick();
                return true;
            }
            return false;
        });

    }

    private void setUpLoggedIn() {
        Bundle bundle = getIntent().getBundleExtra("loginInfo");
        String str;
        if (bundle == null || !bundle.getBoolean("isLoggedIn"))
            str = getString(R.string.not_logged_in);
        else
            str = String.format(Locale.getDefault(), "%s %s", getString(R.string.logged_in_as), bundle.getString("user"));
        tvloggedinas.setText(str);
    }

    /* Get the various views from this activity */
    @Override
    protected void findViews() {
        super.findViews();
        btnsearch = findViewById(R.id.btn_search);
        etsearch = findViewById(R.id.et_search);
        pbsearch = findViewById(R.id.pb_search);
        rvplist = findViewById(R.id.product_list);
        tvloggedinas = findViewById(R.id.tvloggedinas);
    }

    /* Set up the toolbar (or app bar or whatever you like to call it) */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu, new int[]{R.id.search_menu_item});
    }

    /* OnClick listener for the search button */
    public void searchBtnOnClickListener(View v) {

        /* Make sure this is the search button we're dealing with */
        if (v.getId() != R.id.btn_search)
            return;

        /* Initialize the NutriAPI client */
        nc = new AsyncNutriClient();
        nc.setCallback(this::asyncNutriClientCallBack);

        /* Get the search query from the search EditText */
        String query = etsearch.getText().toString();

        /* Hide the recyclerview */
        rvplist.setVisibility(View.GONE);

        /* Show progressbar until the new results arrive */
        pbsearch.setVisibility(View.VISIBLE);


        /* Clear the results from the last search */
        productList.clear();

        /* Tell the RecyclerView that the product list has changed */
        if (rvplist.getAdapter() != null)
            rvplist.getAdapter().notifyDataSetChanged();

        /* Start the request */
        nc.execute(getString(R.string.API_BASEURL), query);

        /* Disble the search button until the request is finished */
        v.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return default_onmenuitemselected(this, item, null);
    }

    private void asyncNutriClientCallBack(Produit[] produits) {

        /* Hide the progressbar */
        pbsearch.setVisibility(View.GONE);

        /* Show the recyclerview */
        rvplist.setVisibility(View.VISIBLE);

        /* Re-enable the search button */
        btnsearch.setEnabled(true);

        /* Handle errors */
        Exception exception = nc.getException();

        if (exception != null) {
            try {
                throw exception;
            } catch (SocketTimeoutException e) {
                Log.d(TAG, "signupCallback: timeout", e);
                toast(getString(R.string.timeout_error), e);
            } catch (IOException e) {
                Log.d(TAG, "signupCallback: io error", e);
                toast(getString(R.string.io_error), e);
            } catch (NullPointerException e) {
                Log.d(TAG, "signupCallback: null argument(s)", e);
                toast(getString(R.string.argument_error), e);
            } catch (NutriAPIException e) {
                Log.d(TAG, "signupCallback: server error", e);
                toast(getString(R.string.server_returned_error), e);
            } catch (Exception e) {
                Log.d(TAG, "signupCallback: unhandled error", e);
                toast(getString(R.string.unhandled_exception), e);
            }
        }

        /* Log the results */
        Log.d(TAG, "ProductList: " + Utils.arrayToString(produits));

        /* Display a toast if no products match the search query */
        if ((produits == null || produits.length == 0) && exception == null)
            toast(getString(R.string.no_results));

        /* Add the results to the product list (which is displayed by the RecyclerView) */
        if (produits == null)
            productList.clear();
        else
            productList.addAll(Arrays.asList(produits));

        /* Tell the RecyclerView that the product list has changed */
        if (rvplist.getAdapter() != null)
            rvplist.getAdapter().notifyDataSetChanged();
    }

    public void recyclerViewItemOnClickListenerOpen(View view) {
        View parent = (View) view.getParent();
        View barcode = parent.findViewById(R.id.rv_item_product_barcode);
        barcode.setVisibility(View.VISIBLE);
        view.setOnClickListener(this::recyclerViewItemOnClickListenerClose);
        ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less));
    }

    private void recyclerViewItemOnClickListenerClose(View view) {
        View parent = (View) view.getParent();
        View barcode = parent.findViewById(R.id.rv_item_product_barcode);
        barcode.setVisibility(View.GONE);
        view.setOnClickListener(this::recyclerViewItemOnClickListenerOpen);
        ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_more));
    }

    private void onRecyclerViewItemClick(int position) {
        Produit produit = productList.get(position);
        toast_short("Clicked item ", position, "\nCode: ", produit);
        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
        Product product = Product.from(produit);
        intent.putExtra(Product.EXTRA_PRODUCT, product);
        startActivity(intent);
    }
}
