package com.example.nutri;

import android.os.AsyncTask;
import android.view.View;

import java.lang.ref.WeakReference;

class AsyncSignupNutriClient extends AsyncTask<String, Void, Exception> {
    private final Callback callback;
    private final WeakReference<View> buttonRef;
    private final String BASE_URI;

    AsyncSignupNutriClient(String baseUri, Callback callback, WeakReference<View> buttonRef) {
        this.BASE_URI = baseUri;
        this.callback = callback;
        this.buttonRef = buttonRef;
    }

    @Override
    protected Exception doInBackground(String... strings) {
        if (strings.length != 4)
            return new IllegalArgumentException("Wrong number of arguments");

        try {
            do_signup(strings[0], strings[1], strings[2], strings[3]);
        } catch (Exception e) {
            return e;
        }

        return null;
    }

    private void do_signup(String user, String name, String mail, String pass)
            throws Exception {
        NutriAPI nutriAPI = new NutriAPI(BASE_URI);
        nutriAPI.signup(user, name, mail, pass);
    }

    @Override
    protected void onPostExecute(Exception e) {
        super.onPostExecute(e);
        callback.callback(e, buttonRef);
    }

    interface Callback {
        void callback(Exception e, WeakReference<View> weakReference);
    }
}
