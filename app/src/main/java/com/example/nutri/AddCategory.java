package com.example.nutri;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.core.widget.ContentLoadingProgressBar;

import com.example.nutri.util.MyAppCompatActivity;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class AddCategory extends MyAppCompatActivity implements AsyncAddCategoryNutriClient.Callbacks {
    private static final String TAG = "AddCategory";

    private EditText catName;
    private EditText catDesc;
    private Button addBtn;
    private ContentLoadingProgressBar pbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        findViews();
        pbar.hide();
        pbar.setVisibility(View.GONE);
    }

    @Override
    protected void findViews() {
        super.findViews();
        catName = findViewById(R.id.et_category_name);
        catDesc = findViewById(R.id.et_category_description);
        addBtn = findViewById(R.id.btn_add_category);
        pbar = findViewById(R.id.pb_add_category);
    }

    public void addCategoryButtonOnClickListener(View view) {
        /* Clever way to find out if one of them is empty */
        if (catName.length() * catDesc.length() == 0) {
            toast(getString(R.string.please_fill_fileds));
            return;
        }

        String n = catName.getText().toString();
        String d = catDesc.getText().toString();

        new AsyncAddCategoryNutriClient(getString(R.string.API_BASEURL), this).execute(n, d);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return default_onmenuitemselected(this, item, null);
    }

    @Override
    public void onPreExecute() {
        pbar.setVisibility(View.VISIBLE);
        pbar.show();
        addBtn.setVisibility(View.GONE);
    }

    @Override
    public void onPostExecute(Exception exception) {
        pbar.hide();
        pbar.setVisibility(View.GONE);
        addBtn.setVisibility(View.VISIBLE);
        if (exception == null) {
            toast_long(getString(R.string.add_category_success));
            return;
        }
        try {
            throw exception;
        } catch (SocketTimeoutException e) {
            Log.d(TAG, "signupCallback: timeout", e);
            toast(getString(R.string.timeout_error), e);
        } catch (IOException e) {
            Log.d(TAG, "signupCallback: io error", e);
            toast(getString(R.string.io_error), e);
        } catch (NullPointerException e) {
            Log.d(TAG, "signupCallback: null argument(s)", e);
            toast(getString(R.string.argument_error), e);
        } catch (NutriAPIException e) {
            Log.d(TAG, "signupCallback: server error", e);
            toast(getString(R.string.server_returned_error), e);
        } catch (Exception e) {
            Log.wtf(TAG, "signupCallback: unhandled error", e);
            toast(getString(R.string.unhandled_exception), e);
        }
    }
}
