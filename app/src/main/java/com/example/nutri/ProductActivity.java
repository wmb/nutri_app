package com.example.nutri;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.nutri.json.Product;
import com.example.nutri.json.Produit;
import com.example.nutri.util.MyAppCompatActivity;

public class ProductActivity extends MyAppCompatActivity {

    private TextView name;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        findViews();

        Intent intent = getIntent();
        if (!intent.hasExtra(Product.EXTRA_PRODUCT)) {
            toast_long("I didn't receive any products!");
            return;
        }

        Produit produit = getIntent().getParcelableExtra(Product.EXTRA_PRODUCT);

        name.setText(produit.getNom());
        description.setText(produit.getDescription());
    }

    @Override
    protected void findViews() {
        super.findViews();
        name = findViewById(R.id.product_name);
        description = findViewById(R.id.product_description);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return set_up_toolbar(this, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return default_onmenuitemselected(this, item, null);
    }
}
