package com.example.nutri.http;

import android.util.Log;

import java.net.HttpCookie;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Store {
    private static final Map<String, List<String>> cookies = new HashMap<>();
    private static final String TAG = "Store";

    public static void add(String uri, List<String> cookies) {
        List<String> list = Store.cookies.get(uri);
        if (list == null) {
            list = new LinkedList<>();
            Store.cookies.put(uri, list);
        }
        try {
            list.addAll(cookies);
        } catch (Exception e) {
            Log.e(TAG, "add: error: ", e);
        }
    }

    private static HttpCookie strToCookie(String str) {
        String[] parts = str.split("=", 2);
        String name = parts[0].trim();
        String value = parts[1].substring(0, parts[1].indexOf(';')).trim();

        Log.d(TAG, "strToCookie: str: " + str);
        Log.d(TAG, "strToCookie: [Cookie: " + name + "=" + value + "]");
        HttpCookie c = null;
        try {
            c = new HttpCookie(name, value);
        } catch (Exception e) {
            Log.e(TAG, "strToCookie: HttpCookie() failed: ", e);
        }
        return c;
    }

    public static List<HttpCookie> getCookies(String url) {
        List<String> cookieList = Store.cookies.get(url);

        if (cookieList == null)
            return null;

        List<HttpCookie> c = new LinkedList<>();
        for (String str : cookieList) {
            HttpCookie cookie = Store.strToCookie(str);
            if (cookie != null)
                c.add(cookie);
        }
        return c;
    }
}
