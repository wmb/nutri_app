package com.example.nutri;

import android.os.AsyncTask;
import android.util.Log;

import com.example.nutri.json.Produit;

import java.io.IOException;
import java.net.SocketTimeoutException;

class AsyncNutriClient extends AsyncTask<String, Void, Produit[]> {

    private final static String TAG = "AsyncNutriClient";
    private Callback<Produit[]> callback;
    private Exception exception;

    AsyncNutriClient() {
        super();
    }

    @SuppressWarnings("unused")
    AsyncNutriClient(Callback<Produit[]> callback) {
        super();
        this.callback = callback;
    }

    @Override
    protected Produit[] doInBackground(String... params) {
        NutriAPI napi = new NutriAPI(params[0]);
        String query = params[1];
        try {
            return napi.produits(query);
        } catch (SocketTimeoutException e) {
            exception = e;
            Log.e(TAG, "Connection timed out", e);
        } catch (IOException e) {
            exception = e;
            Log.e(TAG, "Error with NutriAPI.search", e);
        } catch (NutriAPIException e) {
            exception = e;
            Log.e(TAG, "The returned JSON had an error", e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Produit[] produits) {
        super.onPostExecute(produits);
        callback.callback(produits);
    }

    void setCallback(Callback<Produit[]> callback) {
        this.callback = callback;
    }

    Exception getException() {
        return exception;
    }

    interface Callback<T> {
        void callback(T arg);
    }

}
