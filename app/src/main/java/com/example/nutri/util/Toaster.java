package com.example.nutri.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

class Toaster {
    static final int LONG = Toast.LENGTH_LONG;
    static final int SHORT = Toast.LENGTH_SHORT;
    private static final String TAG = "Toaster";
    private final Toast toast;

    @SuppressLint("ShowToast")
    Toaster(Context context, int length) {
        toast = Toast.makeText(context.getApplicationContext(), TAG, length);
    }

    void toast(Object... strings) {
        if (strings == null || strings.length == 0)
            return;

        StringBuilder sb = new StringBuilder();
        for (Object s : strings) {
            if (s instanceof Exception)
                sb.append(": ");
            sb.append(s);
        }
        toast.setText(sb.toString());
        toast.show();
    }

    void setLength(int length) {
        toast.setDuration(length);
    }
}
