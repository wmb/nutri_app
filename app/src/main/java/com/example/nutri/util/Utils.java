package com.example.nutri.util;

import java.util.List;

public class Utils {

    public static <T> String listToString(List<T> list) {
        if (list == null)
            return "(null)";
        if (list.size() == 0)
            return "(empty)";
        StringBuilder s = new StringBuilder();
        for (T element : list) {
            s.append(element);
            if (list.indexOf(element) < list.size() - 1)
                s.append(", ");
        }
        return s.toString();
    }

    public static <T> String arrayToString(T[] array) {
        if (array == null)
            return "(null)";
        if (array.length == 0)
            return "(empty)";
        StringBuilder s = new StringBuilder();
        int i;
        for (i = 0; i < array.length - 1; ++i) {
            s.append(array[i]);
            s.append(", ");
        }
        s.append(array[i]);
        return s.toString();
    }

}
