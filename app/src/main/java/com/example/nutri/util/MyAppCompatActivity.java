package com.example.nutri.util;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.appcompat.widget.Toolbar;

import com.example.nutri.AddCategory;
import com.example.nutri.LoginActivity;
import com.example.nutri.MainActivity;
import com.example.nutri.R;
import com.example.nutri.SignUpActivity;
import com.google.zxing.integration.android.IntentIntegrator;

public abstract class MyAppCompatActivity extends AppCompatActivity {
    private static final int DEFAULT_MENU = R.menu.default_menu;
    private Toaster toaster;

    protected boolean set_up_toolbar(Context ctx, Menu menu) {
        return set_up_toolbar(ctx, menu, DEFAULT_MENU, null);
    }

    protected boolean set_up_toolbar(Context ctx, Menu menu, int menu_res) {
        return set_up_toolbar(ctx, menu, menu_res, null);
    }

    protected boolean set_up_toolbar(Context ctx, Menu menu, int[] hide_these) {
        return set_up_toolbar(ctx, menu, DEFAULT_MENU, hide_these);
    }

    protected boolean set_up_toolbar(Context ctx, Menu menu, int menu_res, @Nullable int[] hide_these) {
        MenuInflater minflater = new SupportMenuInflater(ctx);
        minflater.inflate(menu_res, menu);
        if (hide_these != null && hide_these.length > 0)
            for (int item : hide_these)
                menu.removeItem(item);
        return true;
    }

    protected boolean default_onmenuitemselected(Context ctx, MenuItem item, @Nullable int[] ignore_these) {
        boolean ret = true;
        int itemId = item.getItemId();

        if (ignore_these != null && ignore_these.length > 0)
            for (int ignore_this : ignore_these)
                if (ignore_this == itemId) {
                    ret = false;
                    break;
                }

        if (!ret)
            return false;

        switch (itemId) {
            case R.id.search_menu_item:
                startActivity(new Intent(ctx, MainActivity.class));
                ret = true;
                break;
            case R.id.scan_menu_item:
                IntentIntegrator ii = new IntentIntegrator((AppCompatActivity) ctx);
                ii.initiateScan();
                ret = true;
                break;
            case R.id.login_menu_item:
                startActivity(new Intent(ctx, LoginActivity.class));
                ret = true;
                break;
            case R.id.new_account_menu_item:
                startActivity(new Intent(ctx, SignUpActivity.class));
                ret = true;
                break;
            case R.id.add_category_menu_item:
                startActivity(new Intent(ctx, AddCategory.class));
                ret = true;
                break;
            case android.R.id.home:
                ((AppCompatActivity) ctx).finish();
                ret = true;
                break;
            default:
                ret = super.onOptionsItemSelected(item);
                break;
        }

        return ret;
    }

    protected void toast(Object... strings) {
        toast_long(strings);
    }

    protected void toast_long(Object... strings) {
        if (toaster == null)
            toaster = new Toaster(this, Toaster.LONG);
        else
            toaster.setLength(Toaster.LONG);
        toaster.toast(strings);
    }

    protected void toast_short(Object... strings) {
        if (toaster == null)
            toaster = new Toaster(this, Toaster.SHORT);
        else
            toaster.setLength(Toaster.SHORT);
        toaster.toast(strings);
    }

    protected void findViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
